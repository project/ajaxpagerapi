This module is meant for Drupal back-end developers, who are working
with custom pages such as reports, content listing using custom queries
etc.

The History Ajax pager enables the user to have ajax pager in custom
listing pages. This Ajax pager also saves the history of the pages using
Benjamin Lupton's History.js.

Installation:

  1. Download the latest version of History.js
     (from https://github.com/balupton/History.js)

  2. Extract the contents of the archive to:
     <Drupal>/sites/<example.com | all>/libraries/balupton-history.js

     Such that the bundled directory exists at:
     <...>/libraries/balupton-history.js/scripts/bundled


  3. Verify You have the Libraries API installed

  4. Disable the Overlay Module

Usage :

Instead of using normal drupal pager, we can use the ajax pager.

Normal Drupal pager usage is as follow:

$output['pager'] = array(
    '#theme' => 'pager',
  );

Instead of this you can use the following code:

$output['pager'] = array(
  '#markup' => theme('ajax_pager', array(
    'parameters' => array(
      'selector' => 'block-system-main',
))));

Here selector is the id of the div, on which the content is getting
displayed.
