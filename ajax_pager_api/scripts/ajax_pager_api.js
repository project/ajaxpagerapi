/**
 * @file
 * This file will contain the browser history related js code for ajax links.
 */

(function ($) {
  $.fn.pager_history = function(pager) {
    var origTitle = document.title;
    if (!History.enabled) {
      return false;
    }
    var page = Number(pager);
    var query = window.location.search.substring(1);
    page = "?page=" + page;
    if (query) {
      var placeHolder = "page";
      var pageindex = query.indexOf(placeHolder);
      if (pageindex >= 0) {
        query = query.substr(0, query.indexOf(placeHolder) - 1);
      }
      if (query) {
        page = "?" + query + "&page=" + page;
      }
    }
    var obj = "";
    manualStateChange = false;
    history.pushState(obj, origTitle, page);
  };
  // Prepare.
  // Note: We are using a capital H instead of a lower h.
  var History = window.History;
  if (!History.enabled) {
       // History.js is disabled for this browser.
       // This is because we can optionally choose to support HTML4 browsers or not.
      return false;
  }

  // Bind to StateChange Event.
  // Note: We are using statechange instead of popstate.
  window.addEventListener("popstate", function(e) {
    var view = "";
    // URL location.
    var location = document.location;
    var State = e.state;
    // View mode.
    if (State) {
      view = State.view;
    }
    // Instead of the line above, you could run the code below if the url returns the whole page instead of just the content (assuming it has a `#content`).
    $.get(location.href, function(response) {
      var selector = Drupal.settings.ajax_pager.selector;
      selector = '#' + selector;
      // Replaces the main content.
      $(selector).html($(response).find(selector).html());
      // Replaces the pager.
      $('.pager').html($(response).find('.pager').html());
      addajax();
      addctools();
      var title = $(response).filter('title').text();
      // Change the title of the document.
      $('title').text(title);
    });
  });

  // Function for adding ajax to dynamically loaded content. As drupal defaultly adds it on document ready event.
  function addajax() {
    // Bind Ajax behaviors.
    jQuery('.use-ajax:not(.ajax-processed)').addClass('ajax-processed').each(function ()    {
      var element_settings = {};
      // Clicked links look better with the throbber than the progress bar.
      element_settings.progress = { 'type': 'throbber' };

      // For anchor tags, these will go to the target of the anchor rather
      // than the usual location.
      if (jQuery(this).attr('href')) {
        element_settings.url = jQuery(this).attr('href');
        element_settings.event = 'click';
      }
      var base = jQuery(this).attr('id');
      Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
    });
  }

  // Function for adding ajax to dynamically loaded content. As drupal defaultly adds it on document ready event.
  function addctools() {
    $('area.ctools-use-modal, a.ctools-use-modal').each(function() {
      var $this = $(this);
      $this.unbind();
      $this.click(Drupal.CTools.Modal.clickAjaxLink);
      // Create a drupal ajax object.
      var element_settings = {};
      if ($this.attr('href')) {
        element_settings.url = $this.attr('href');
        element_settings.event = 'click';
        element_settings.progress = {
          type: 'throbber'
        };
      }
      var base = $this.attr('href');
      Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
    });
  }
})(jQuery);
